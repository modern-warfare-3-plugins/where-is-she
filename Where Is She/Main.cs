using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;
using Addon;

namespace WhereIsShe
{
	public class Main : CPlugin
	{
		private IntPtr _activate, _icon, _originX, _originY;
		private float _x, _y;
		private Timer _lastManStanding;
		private Stopwatch _stopWatch;
		private Configuration _configuration;
		private readonly GameOptions _gameStats = new GameOptions();

		/// <summary>
		/// Executes when the MW3 server loads
		/// </summary>
		public override void OnServerLoad()
		{
			_configuration = new Configuration
			{
				IconNumber = int.Parse(GetServerCFG("WhereIsShe", "Icon", "40")),
				GirlyTime = int.Parse(GetServerCFG("WhereIsShe", "Timer", "30")),
				DetectMovement = bool.Parse(GetServerCFG("WhereIsShe", "DetectMovement", "false")),
				Threshold = int.Parse(GetServerCFG("WhereIsShe", "Threshold", "30")),
				ResetTime = int.Parse(GetServerCFG("WhereIsShe", "ResetTime", "5"))
			};

			// Memory pointers found by 8q4s8
			_activate = (IntPtr) Convert.ToInt32(0x01B1EE9C);
			_icon = (IntPtr) Convert.ToInt32(0x01B1EEBC);
			_originX = (IntPtr) Convert.ToInt32(0x01B1EEA0);
			_originY = (IntPtr) Convert.ToInt32(0x01B1EEA4);

			// Set the initial icon
			ChangeIcon();

			// Create a new stop watch so we can track the amount of time that the girl has been camping
			// in a corner for
			_stopWatch = new Stopwatch();

			// Create a new timer which can be re-used as many times as its needed
			_lastManStanding = new Timer
			{
				Enabled = false,
				Interval = 500
			};

			_lastManStanding.Elapsed += FindTheGirl;

			// Setup messages
			ServerPrint("\n[Where Is She] Created by SgtLegend. Version 0.3 BETA");
			ServerPrint("[Where Is She] Code from waypoint plugin created by 8q4s8");
		}

		/// <summary>
		/// Executes when the addon frame base method gets called
		/// </summary>
		public override void OnAddonFrame()
		{
			try
			{
				List<ServerClient> clients = new List<ServerClient>(GetClients());

				// Update the total number of clients on the server
				_gameStats.ClientsTotal = clients.Count;
				_gameStats.ClientsInfected = 0;
				_gameStats.ClientsHuman = 0;

				// Go through all the clients on the server and increment the total number of infected
				// players so can determine if there is one player left alive
				if (clients.Count == 0) return;

				foreach (ServerClient client in clients)
				{
					switch (client.Team)
					{
						case Teams.Axis:
							_gameStats.ClientsInfected++;
							break;

						case Teams.Allies:
							_gameStats.ClientsHuman++;
							break;
					}
				}

				// Now check the number of infected players against the number of humans remaining in the
				// game, if the total number of clients minus the total number of zombies equals one then
				// we can be sure that we need to seek out the girl
				if ((_gameStats.ClientsTotal - _gameStats.ClientsInfected) > 1 || _gameStats.ClientsHuman > 1)
				{
					UpdateMiniMapIcon(true);
					return;
				}

				foreach (ServerClient client in clients)
				{
					if (client.Team != Teams.Allies || client.SpectateType == SpectateTypes.None) continue;

					// Set who the girl is and start the timer
					if (_gameStats.TheGirl == null)
					{
						_gameStats.TheGirl = client;
						_stopWatch.Start();
						_lastManStanding.Start();
					}

					// Set the clients position for the mini map icon
					_x = client.OriginX;
					_y = client.OriginY;

					// Check if the clients last known position is different from their current position
					if (_gameStats.LastKnownPosition != null)
					{
						_gameStats.IsNotMovingAround = !IsClientMoving(client);
					}

					// Update the icon details
					ChangeIcon();
					SetPosition();

					// Set the players last known position in the map
					_gameStats.LastKnownPosition = new Vector(client.OriginX, client.OriginY, client.OriginZ);

					break;
				}
			}
			catch
			{
				// Doesn't get used, its here to simply catch any errors that occur while the MW3 server
				// is booting up or changing maps
			}
		}

		/// <summary>
		/// Executes when the map changes
		/// </summary>
		public override void OnMapChange()
		{
			UpdateMiniMapIcon(true);
		}

		/// <summary>
		/// Executes when the current game map restarts
		/// </summary>
		public override void OnFastRestart()
		{
			UpdateMiniMapIcon(true);
		}

		/// <summary>
		/// Executes when a player types something into the game chat
		/// </summary>
		/// <param name="message"></param>
		/// <param name="client"></param>
		/// <param name="teamchat"></param>
		/// <returns></returns>
		public unsafe override ChatType OnSay(string message, ServerClient client, bool teamchat)
		{
			string[] command = message.Split(' ');

			if (command.Length == 0 || command[0] == null || !command[0].Trim().StartsWith("!icon"))
			{
				return ChatType.ChatContinue;
			}

			// Parse the given icon number if one was given otherwise let the client know they did
			// something when typing the "!icon" command
			if (command[0] == "!icon" && (command[1] == null || command[1].Length == 0 || !int.TryParse(command[1], out _configuration.IconNumber)))
			{
				TellClient(client.ClientNum, "^1Invalid icon number, E.g. ^7!icon 1", true);
			}
			else
			{
				ChangeIcon();
			}

			// DEBUGGING, NOT TO BE USED DURING REAL GAMES
			if (command[0] == "!icontoggle")
			{
				_x = client.OriginX;
				_y = client.OriginY;

				// Update the icon details
				ChangeIcon();
				SetPosition();

				// Set the icons status
				IconStatus(*(int*) _activate == 1 ? 0 : 1);
			}

			return ChatType.ChatNone;
		}

		/// <summary>
		/// Updates the position of the mini map icon given that is there only one player on the allies team
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FindTheGirl(object sender, ElapsedEventArgs e)
		{
			if (_gameStats.TheGirl == null)
			{
				return;
			}

			double elasped = Math.Abs(_stopWatch.Elapsed.TotalSeconds);

			// Check if we need to detect for movement, if there hasn't been any movement for the time period set
			// and the stop watch has reached its goal or if we don't need to detect for movement show the icon on
			// the mini map
			if ((_configuration.DetectMovement && _gameStats.IsNotMovingAround && elasped >= _configuration.ResetTime) ||
				elasped >= _configuration.GirlyTime)
			{
				ChangeIcon();
				IconStatus(1);
				UpdateMiniMapIcon(false);
			}
			else if (_configuration.DetectMovement && !_gameStats.IsNotMovingAround)
			{
				IconStatus(0);
				_stopWatch.Reset();
				_stopWatch.Start();
			}
		}

		/// <summary>
		/// Deactivates the current mini map icon and resets the last man standing timer for the next game
		/// </summary>
		private void UpdateMiniMapIcon(bool deactivate)
		{
			_gameStats.ClientsHuman = 0;
			_gameStats.ClientsInfected = 0;
			_gameStats.ClientsTotal = 0;

			// Deactivate the mini map icon
			if (deactivate)
			{
				IconStatus(0);
			}

			// Stop the timers
			if ((!deactivate && !_configuration.DetectMovement) || deactivate)
			{
				_stopWatch.Reset();
				_stopWatch.Stop();
				_lastManStanding.Stop();

				_gameStats.IsNotMovingAround = false;
				_gameStats.TheGirl = null;
			}
		}

		/// <summary>
		/// Determines if the client is moving around
		/// </summary>
		/// <param name="client"></param>
		/// <returns></returns>
		private bool IsClientMoving(ServerClient client)
		{
			return Math.Abs(Math.Abs(client.OriginX - _gameStats.LastKnownPosition.X)) >= _configuration.Threshold ||
				Math.Abs(Math.Abs(client.OriginY - _gameStats.LastKnownPosition.Y)) >= _configuration.Threshold ||
				Math.Abs(Math.Abs(client.OriginZ - _gameStats.LastKnownPosition.Z)) >= _configuration.Threshold;
		}

		/// <summary>
		/// Activates/deactivates the icon on the mini map
		/// </summary>
		public unsafe void IconStatus(int status)
		{
			*(int*) _activate = status;
		}

		/// <summary>
		/// Changes the icon on the mini map
		/// </summary>
		public unsafe void ChangeIcon()
		{
			*(int*) _icon = _configuration.IconNumber;
		}

		/// <summary>
		/// Sets the position of the icon on the mini map
		/// </summary>
		public unsafe void SetPosition()
		{
			*(float*) _originX = _x;
			*(float*) _originY = _y;
		}
	}

	internal class GameOptions
	{
		public int ClientsTotal = 0;
		public int ClientsInfected = 0;
		public int ClientsHuman = 0;
		public bool IsNotMovingAround = false;
		public ServerClient TheGirl;
		public Vector LastKnownPosition;
	}

	internal class Configuration
	{
		public int IconNumber = 40;
		public int GirlyTime = 30;
		public bool DetectMovement = true;
		public int Threshold = 30;
		public int ResetTime = 5;
	}
}